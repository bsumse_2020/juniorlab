import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
import os

# Conversion factors
lbs_to_N = float(4.45) # N/Lb
inch_to_mm = float(25.4) # mm/inch
inch_to_m = float(0.0254) # m/inch
sqinch_to_sqm = float(0.00064516) # sq meters/sq inch
pi = math.pi

def get_data(filename): # Gets data and plots it from raw data file

    # Opens excel file and makes list of tabs in the file
    excelFile = pd.ExcelFile(filename)
    sheets = excelFile.sheet_names # List of tabs/sheets in the excel file
 
    # Plot Setup
    fig = plt.figure()
    plt.title('Stress vs. Strain')
    plt.ylabel('Stress (MPa)')
    plt.xlabel('Strain')
    ax = plt.gca()
    ax.minorticks_on()
    ax.grid(which='major',linestyle='-',linewidth=.40)
    ax.grid(which='minor',linestyle='-',linewidth=.15)

    # Grabs the data from the spreadsheets
    for sheet in sheets:
        excel_file = pd.read_excel(excelFile,sheet_name = sheet)
        test_num = (np.array(excel_file['TESTNUM'])[0])

        # User inputs for the diameter and gauge length for each sample
        print('Enter the diameter (in inches) for sample {} (Test# {}) or enter S to skip:'.format(sheet,test_num))
        ans = input()
        if ans == 's' or ans == 'S':
           print() 
        else:
            diameter_in = float(ans)
            print('Enter the sample gauge length (in inches) for {}'.format(sheet))
            gauge_in = float(input())
            diameter_m = diameter_in*inch_to_m
            radius_m = diameter_m/2
            gauge_mm = gauge_in*inch_to_mm
            area_m = pi*radius_m**2 # cross sect. area in square meters

            # Force --> Stress
            force_lbs = np.array(excel_file['FORCE']) # Force column in spreadsheet
            force_newt = (force_lbs*lbs_to_N)
            stress_Pa = force_newt/area_m
            stress_MPa = stress_Pa/1000000 # Stress in MPa
        
            # Position --> Strain
            position = np.array(excel_file['POSIT']) # Position column in spreadsheet
            position_mm = (position*inch_to_mm)
            position_initial = position_mm[0]
            position_delta = position_mm-position_initial
            strain = position_delta/gauge_mm # Strain (unitless)

            ax.plot(strain, stress_MPa, label=str(sheet)) # Plots data

    ax.legend()
    plt.show()

a = 1
while a == 1:
    print('Enter the file name you want to open (Caps sensitive) or enter Q to quit:')
    fname = input()
    if fname == 'Q' or fname == 'q':
        a = 0
        break
    else:
        a = 1

    if os.path.exists('{}.xls'.format(fname)):
        get_data('{}.xls'.format(fname))
        print('Do you want to plot another file? [y/n]')
        ans = input.lower()
        if ans == 'y':
            a = 1
        else:
            a = 0
    elif os.path.exists('{}.xlsx'.format(fname)):
        get_data('{}.xlsx'.format(fname))
        print('Do you want to plot another file? [y/n]')
        ans = input().lower()
        if ans == 'y':
            a = 1
        else:
            a = 0
            break

    else:
        print('That file does not exist')
        print('Double check to make sure the file is in the same folder, and there are no typos.')
        print()
