# Junior Lab Repository
---
### Stress-Strain Project

This program will take in the data that is output from the tensile testing machine in the 380/381 lab. The program will ask for multiple data sets and will plot each of those data sets on the same graph. Units are in MPa.

---
# Conact information
---
Tyler Webb  - tylerwebb508@u.boisestate.edu</br>
Chris Jones - chrisjones4@u.boisestate.edu